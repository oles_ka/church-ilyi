var gulp = require('gulp');
var sass = require('gulp-sass');
var webserver = require('gulp-webserver');

gulp.task('webserver', function() {
    gulp.src('')
        .pipe(webserver({
            livereload: true,
            directoryListing: true,
            open: true,
            port: 8080,
            fallback: 'index.html'
        }));
});

gulp.task('scss', function() {
    gulp.src('css/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('css/'));
});

gulp.task('watch', function() {
    gulp.watch('css/scss/*.scss',['scss']);
});
gulp.task('default', ['watch', 'scss', 'webserver']);